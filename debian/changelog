msolve (0.7.4-1) UNRELEASED; urgency=medium

  * New upstream release.

 -- Doug Torrance <dtorrance@piedmont.edu>  Fri, 28 Feb 2025 06:30:40 -0500

msolve (0.7.3-1) unstable; urgency=medium

  * New upstream release.

 -- Doug Torrance <dtorrance@debian.org>  Mon, 14 Oct 2024 07:10:20 -0400

msolve (0.7.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/libmsolve0.symbols
    - Update symbols for new release.

 -- Doug Torrance <dtorrance@debian.org>  Thu, 19 Sep 2024 22:19:26 -0400

msolve (0.7.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/32-bit.patch
    - Remove patch; applied upstream.

 -- Doug Torrance <dtorrance@debian.org>  Wed, 31 Jul 2024 09:33:31 -0400

msolve (0.7.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/32-bit.patch
    - Completely rewrite patch.  Previous version was applied upstream,
      and now we fix a completely different but new issue that was causing
      compilation errors on 32-bit machines.

 -- Doug Torrance <dtorrance@debian.org>  Tue, 30 Jul 2024 13:31:35 -0400

msolve (0.6.8-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/32-bit.patch
    - New patch to fix 32-bit builds.

 -- Doug Torrance <dtorrance@debian.org>  Thu, 25 Jul 2024 23:09:51 -0400

msolve (0.6.7-1) unstable; urgency=medium

  * New upstream release.
  * debian/control
    - Remove unused ${shlibs:Depends} from Depends for libmsolve-dev.
  * debian/libmsolve0.symbols
    - Update symbols for new release.
    - Add Build-Depends-Package fields.

 -- Doug Torrance <dtorrance@debian.org>  Fri, 19 Jul 2024 17:17:08 -0400

msolve (0.6.6-1) unstable; urgency=medium

  * Upload to unstable.
  * Source-only upload for transition to testing.

 -- Doug Torrance <dtorrance@debian.org>  Thu, 27 Jun 2024 14:13:43 -0400

msolve (0.6.6-1~exp1) experimental; urgency=medium

  * New upstream release.
  * debian/control
    - Bump Standards-Version to 4.7.0.
    - Rename libmsolve-0.6.5 binary package to libmsolve0 now that the
      libraries are built with -version-info instead of -release.
  * debian/libmsolve-dev.install
    - Simplify wildcards.
  * debian/libmsolve0.install
    - Rename from libmsolve-0.6.5.install and update wildcards.
  * debian/libmsolve0.symbols
    - Rename from libmsolve-0.6.5.symbols and add one new symbol.

 -- Doug Torrance <dtorrance@debian.org>  Thu, 27 Jun 2024 12:04:55 -0400

msolve (0.6.5-1) unstable; urgency=medium

  * Upload to unstable.
  * Source-only upload for transition to testing.
  * debian/libmsolve-0.6.5.symbols
    - Mark _mod_mat_addmul_transpose_op as architecture-specific; only
      available on x86 processors.

 -- Doug Torrance <dtorrance@debian.org>  Tue, 27 Feb 2024 16:56:51 -0500

msolve (0.6.5-1~exp1) experimental; urgency=medium

  * New upstream release.
  * debian/control
    - Rename libmsolve-0.6.4 package to libmsolve-0.6.5.
    - Add Section for libmsolve-0.6.5 and libmsolve-dev binary
      packages (libs and libdevel, respectively).
  * debian/libmsolve-0.6.5.install
    - Rename from libmsolve-0.6.4.install.
  * debian/libmsolve-0.6.5.symbols
    - Add symbols file.

 -- Doug Torrance <dtorrance@debian.org>  Mon, 26 Feb 2024 18:19:21 -0500

msolve (0.6.4-1) unstable; urgency=medium

  * Upload to unstable.
  * Source-only upload for transition to testing.

 -- Doug Torrance <dtorrance@debian.org>  Sun, 11 Feb 2024 21:14:13 -0500

msolve (0.6.4-1~exp1) experimental; urgency=medium

  * New upstream release.
  * debian/control
    - Add new libmsolve-0.6.4 and libmsolve-dev packages containing the
      shared libraries and development files, respectively.
  * debian/libmsolve-0.6.4.install
    - New file; install shared libraries.
  * debian/libmsolve-dev.install
    - New file; install development files.
  * debian/lintian-overrides
    - Remove file; no longer necessary now that we install the shared
      libraries in their own package.
  * debian/msolve.doc-base
    - Rename from "doc-base".
  * debian/msolve.docs
    - Rename from "docs".
  * debian/msolve.install
    - Rename from "install" and install contents of usr/bin.
  * debian/msolve.manpages
    - Rename from "manpages".
  * debian/not-installed
    - Don't install libtool .la files.
  * debian/rules
    - Stop removing files that now are installed in -dev package.
    - Update paths for generating manpage (debian/msolve -> debian/tmp).

 -- Doug Torrance <dtorrance@debian.org>  Sat, 10 Feb 2024 10:11:28 -0500

msolve (0.6.3-2) unstable; urgency=medium

  * debian/copyright
    - Bump my copyright years.
  * debian/rules
    - Don't install headers or pkg-config file.

 -- Doug Torrance <dtorrance@debian.org>  Thu, 04 Jan 2024 17:58:46 -0500

msolve (0.6.3-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/skip-tests.patch
    - Remove patch; applied upstream

 -- Doug Torrance <dtorrance@debian.org>  Sat, 16 Dec 2023 16:12:52 -0500

msolve (0.6.2-2) unstable; urgency=medium

  * debian/patches/skip-tests.patch
    - New patch; skips failing tests.

 -- Doug Torrance <dtorrance@debian.org>  Sat, 16 Dec 2023 14:31:18 -0500

msolve (0.6.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/lintian-overrides
    - Update version number.
  * debian/patches/xcolor-hyperref.patch
    - Remove patch; applied upstream.

 -- Doug Torrance <dtorrance@debian.org>  Fri, 15 Dec 2023 08:04:00 -0500

msolve (0.6.1-2) unstable; urgency=medium

  * debian/lintian-overrides
    - Update version number in override.
  * debian/patches/xcolor-hyperref.patch
    - New patch; fix spelling of "hyperref" option to xcolor package
      (Closes: #1058557).

 -- Doug Torrance <dtorrance@debian.org>  Wed, 13 Dec 2023 09:43:55 -0500

msolve (0.6.1-1) unstable; urgency=medium

  * New upstream release.

 -- Doug Torrance <dtorrance@debian.org>  Thu, 02 Nov 2023 16:00:15 -0400

msolve (0.6.0-1) unstable; urgency=medium

  * New upstream release.

 -- Doug Torrance <dtorrance@debian.org>  Mon, 30 Oct 2023 12:08:44 -0400

msolve (0.5.0-2) unstable; urgency=medium

  * debian/lintian-overrides
    - Update version number in package-name-doesnt-match-sonames override.
  * debian/rules
    - Skip tests on 32-bit architectures to prevent FTBFS.

 -- Doug Torrance <dtorrance@debian.org>  Thu, 13 Jul 2023 12:34:36 -0400

msolve (0.5.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/fix-typos.patch
    - Remove patch; applied upstream.

 -- Doug Torrance <dtorrance@debian.org>  Wed, 05 Jul 2023 10:57:46 -0400

msolve (0.4.9-3) unstable; urgency=medium

  * debian/tests/control
    - Don't run autopkgtests on 32-bit architectures.

 -- Doug Torrance <dtorrance@debian.org>  Tue, 20 Jun 2023 11:04:39 -0400

msolve (0.4.9-2) unstable; urgency=medium

  * Source-only upload for transition to testing (after bookworm release).
  * debian/gbp.conf
    - New file; set default branch to debian/latest.

 -- Doug Torrance <dtorrance@debian.org>  Fri, 19 May 2023 14:41:20 -0400

msolve (0.4.9-1) unstable; urgency=medium

  * Initial release (Closes: #1034619).

 -- Doug Torrance <dtorrance@debian.org>  Thu, 20 Apr 2023 23:17:49 -0400
